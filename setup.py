"""
"""

import sys
__title__ = 'simple_kms'
__version__ = '0.1.1'
__author__ = 'Xianhao.lin'
__author_email__ = 'xianhao.lin@baysensors.com'
try:
    from setuptools import setup, find_packages
except ImportError:
    print('%s now needs setuptools in order to build.' % __title__)
    print('Install it using your package manager (usually python-setuptools) or via pip \
            (pip install setuptools).')
    sys.exit(1)

setup(
        name=__title__,
        version=__version__,
        author=__author__,
        author_email=__author_email__,
        install_requires=[
            'google-api-python-client',
            'google-auth',
            'google-auth-httplib2',
            'pycrypto',
            'pyasn1',
            'pyasn1-modules'
        ],
        package_dir={__title__: __title__},
        packages=find_packages('.'),
        test_suite="test",
        license="Percolata Corp. 2017",
        scripts=[],
        include_package_data=True,
        entry_points={
            'console_scripts': [
                'simple_kms=simple_kms.encryption:main',
                ],
            },
        zip_safe=False
        )
