
### Pre-Requisites
Install the following on the machine that tool is running
1. Python 3 or later version, instructions can be found [here](https://cloud.google.com/python/setup#installing_python)
2. apt-get install python3-dev
3. Install pip3 from the python3 package
  ```bash
  sudo apt install python3-pip
  ```
4. Install the requirements using pip3
  ```bash
  sudo pip3 install -r requirements.txt
  ```
5. A service account that has cloud KMS encryptor decryptor scopes in your GCP project to encrypt the DEK(Data Encryption Key)
You can get it from `Google Cloud Platform -> IAM & admin -> Service accounts`.

### Install
python3 setup.py install

## Usage
1. To encrypt any config file in any directory.  
  ```
  directory: simple_kms encrypt -d  <directory name of config file> -e <directory name of encrypted file> -k <service account with KMS access>
  file: simple_kms encrypt -df  <config file path> -ef <encrpted file path> -k <service account with KMS access>
  ```
2. To decrypt any already encrypted files.  
  ```
  directory: simple_kms decrypt -d  <directory name of config file> -c deploy.config -e <directory name of encrypted file> -k <service account with KMS access>
  file: simple_kms decrypt -df  <config file path> -ef <encrpted file path> -k <service account with KMS access>
  ```
