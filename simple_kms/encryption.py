#! /usr/bin/python3

import os
import base64
import json
import sys
import argparse
import io
import googleapiclient.discovery
from google.oauth2 import service_account

PROJECT_ID = "faas-prod"
LOCATION_ID = "global"
KEY_RING_ID = "dev"
CRYPTO_KEY_ID = "code"


def encrypt(directory, encryptdir, keyfile, encrypted_file, decrypted_file):
    if encryptdir and not os.path.isdir(encryptdir):
        os.mkdir(encryptdir)
    if not decrypted_file:
        encrypt_list = [[os.path.join(directory, fl), os.path.join(encryptdir, "%s.encrypted" % fl)] for fl in os.listdir(directory)]
    else:
        encrypt_list = [[decrypted_file, encrypted_file]]

    for decrypted_file, encrypted_file in encrypt_list:
        plaintext_file_name = decrypted_file
        if os.path.isdir(plaintext_file_name):
            print('Error: , %s is a folder, can not be encrypted, skip...' % plaintext_file_name)
            continue
        # service_account_key = os.path.join(directory,keyfile)

        credentials = service_account.Credentials.from_service_account_file(keyfile)
        # Creates an API client for the KMS API.
        kms_client = googleapiclient.discovery.build('cloudkms', 'v1', credentials=credentials)

        # The resource name of the CryptoKey.
        name = 'projects/{}/locations/{}/keyRings/{}/cryptoKeys/{}'.format(
            PROJECT_ID, LOCATION_ID, KEY_RING_ID, CRYPTO_KEY_ID)

        with io.open(plaintext_file_name, 'rb') as plaintext_file:
            plaintext = plaintext_file.read()
            plaintext = plaintext.strip()

        # Use the KMS API to encrypt the data.
        crypto_keys = kms_client.projects().locations().keyRings().cryptoKeys()
        request = crypto_keys.encrypt(
            name=name,
            body={'plaintext': base64.b64encode(plaintext).decode('ascii')})
        response = request.execute()
        ciphertext = base64.b64decode(response['ciphertext'].encode('ascii'))

        ciphertext_file_name = encrypted_file

        with io.open(ciphertext_file_name, 'wb+') as ciphertext_file:
            ciphertext_file.write(ciphertext)

        print('Saved ciphertext to {}.'.format(ciphertext_file_name))


def decrypt(encryptdir, keyfile, directory, encrypted_file, decrypted_file):
    if directory and not os.path.isdir(directory):
        os.makedirs(directory)
    if not decrypted_file:
        encrypt_list = [[os.path.join(directory, os.path.splitext(fl)[0]), os.path.join(encryptdir, fl)] for fl in os.listdir(encryptdir)]
    else:
        encrypt_list = [[decrypted_file, encrypted_file]]

    for decrypted_file, encrypted_file in encrypt_list:
        print("process %s" % decrypted_file)
        ciphertext_file_name = encrypted_file
        plaintext_file_name = decrypted_file
        # service_account_key = os.path.join(directory,keyfile)
        credentials = service_account.Credentials.from_service_account_file(keyfile)

        # Creates an API client for the KMS API.
        kms_client = googleapiclient.discovery.build('cloudkms', 'v1', credentials=credentials)

        # The resource name of the CryptoKey.
        name = 'projects/{}/locations/{}/keyRings/{}/cryptoKeys/{}'.format(
            PROJECT_ID, LOCATION_ID, KEY_RING_ID, CRYPTO_KEY_ID)

        if not os.path.exists(ciphertext_file_name):
            print("Error: File not found {}".format(ciphertext_file_name))
            sys.exit()

        # Read encrypted data from the input file.
        with io.open(ciphertext_file_name, 'rb') as ciphertext_file:
            ciphertext = ciphertext_file.read()
        # Use the KMS API to decrypt the data.
        crypto_keys = kms_client.projects().locations().keyRings().cryptoKeys()
        request = crypto_keys.decrypt(
            name=name,
            body={'ciphertext': base64.b64encode(ciphertext).decode('ascii')})
        response = request.execute()
        if response == {}:
            print("Empty file: %s" % plaintext_file_name)
            plaintext = b""
        else:
            plaintext = base64.b64decode(response['plaintext'].encode('ascii'))

        # Print the decrypted data for demo

        with io.open(plaintext_file_name, 'wb') as deciphertext_file:
            deciphertext_file.write(plaintext)
        print("decrypt ok: %s" % plaintext_file_name)
        # os.remove(ciphertext_file_name)


def main():
    """creates the parser and takes the arguments form command line
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('command', help='encrypt or decrypt', choices=["encrypt", "decrypt"], type=str)
    parser.add_argument('-d', '--directory', required=False, help="the directory to be encrypted."
                        "it's also the directory that save the decrypted files.",
                        default=None)
    parser.add_argument('-ef', '--encrypted_file', required=False,
                        help="target encrypted file, leave it null if want to encrypt the whole folder.",
                        default=None)
    parser.add_argument('-df', '--decrypted_file', required=False,
                        help="target decrypted file, leave it null if want to decrypt the whole folder.",
                        default=None)
    parser.add_argument('-e', '--encryptdir', required=False, help="the directory to save the encrypted files")
    parser.add_argument('-k', '--keyfile', required=True, help="key file is the google service account to access KMS, "
                                                               "you can get it from "
                                                               "Google Cloud Platform -> IAM & admin -> "
                                                               "Service accounts",
                        default=None)
    args = parser.parse_args()

    if args.command == 'encrypt':
        encrypt(
            args.directory,
            args.encryptdir,
            args.keyfile,
            args.encrypted_file,
            args.decrypted_file,

        )
    elif args.command == 'decrypt':
        decrypt(
            args.encryptdir,
            args.keyfile,
            args.directory,
            args.encrypted_file,
            args.decrypted_file,
        )
    elif args.command is None:
        parser.print_help()


if __name__ == '__main__':
    main()
